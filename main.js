
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("gif", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("gif");
    ev.target.appendChild(document.getElementById(data));
}

var coords = document.getElementById("Geolocation");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    else {
        coords.innerHTML = "Browser doesn't support geolocation";
    }
}

function showPosition(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    coords.innerHTML = "Latitude of location: " + latitude + "<br>Longitude of location: " + longitude;

    if (typeof (storage) !== "undefined") {
        localStorage.setItem('Latitude', latitude);
        localStorage.setItem('Longitude', longitude);
        console.log(localStorage);
    }
    else {
        document.getElementById("LocationStorage").innerHTML = "Browser doesn't support web storage";
    }
}
